#!/usr/bin/env python3

import os
import cgi, cgitb 
import subprocess
#from subprocess import run

def print_code(x):
	print("<code>\n")
	print(x)
	print("\n</code>")

print("Content-type:text/html\r\n\r\n")
print("""<head><meta charset='utf-8'><title>Print Status</title>
<style>	
	body {
		max-width: 600px;
		margin: auto;
		padding: 10px;
		font-family: sans-serif;
		text-align: center;
	}
	h1 {
		font-size: 20pt;
		font-family: open-sans sans-serif;
	}
</style>
</head>
""")
try:
	form = cgi.FieldStorage()
	uploaded_file = form.getvalue('to-print')
	with open("to-print", "wb") as temp_file:
		temp_file.write(uploaded_file)

	output = subprocess.check_output(["lpr", "to-print"], universal_newlines=True)

	print("<p>File received. Your document should print shortly. <a href='index.cgi'>Back</a></p>")
	print_code(output)
except subprocess.CalledProcessError as e:
	print("<h1>CalledProcessError</h1>")
	print(str(e.output, 'utf-8'))
except Exception as e:
	print("<h1>Error</h1>")
	print_code(e)

