#!/usr/bin/env python3

import cgi, cgitb 
import os
from string import Template

print("Content-type:text/html\r\n\r\n")

try:
	with open("index-content.html", "r") as f:
		template = Template(f.read()).substitute({
			'SERVER_NAME': str(os.environ.get("SERVER_NAME"))
		})
		print(template)

except Exception as e:
	print(e)
